#!/usr/bin/python2

import os
import shutil
import argparse

file_maps = {
	"alacritty.yml": "~/.config/alacritty/alacritty.yml",  
	".Xresources": "~/.Xresources",  
	".zshrc": "~/.zshrc"
}


def wrapper(func):
	def _wrapper(*args, **kwargs):
		args = [os.path.expanduser(arg) for arg in args]
		print "copying %s to %s..." % (args[0], args[1])
		return func(*args, **kwargs)
	return _wrapper


shutil.copy2 = wrapper(shutil.copy2)


def main():
	argparser = argparse.ArgumentParser()
	argparser.add_argument("-d", "--dump", help="dump the config files", action="store_true")
	argparser.add_argument("-r", "--restore", help="restore the config files", action="store_true")
	argparser.add_argument("file", help="specify the file you want to restored", nargs='*', default=None)
	args = argparser.parse_args()
	if args.dump:
		if not args.file:
			for k, v in file_maps.iteritems():
				shutil.copy2(v, k)
		else:
			for k, v in file_maps.iteritems():
				if k in args.file:
					shutil.copy2(v, k)
	else:
		if not args.file:
			for k, v in file_maps.iteritems():
				shutil.copy2(k, v)
		else:
			for k, v in file_maps.iteritems():
				if k in args.file:
					shutil.copy2(k, v)


if __name__ == '__main__':
	main()


	



